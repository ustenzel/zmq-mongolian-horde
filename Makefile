CFLAGS= -Wall 

CFLAGS+= $(shell pkg-config libzmq --cflags)
LDFLAGS+= $(shell pkg-config libzmq --libs)

all: demo

demo: demo.o zmq_horde.o
	$(CC) $(LDFLAGS) $^ -o $@ -lzmq -lpthread -lc

demo.o: zmq_horde.h
zmq_horde.o: zmq_horde.h

clean:
	-rm demo.o zmq_horde.o demo

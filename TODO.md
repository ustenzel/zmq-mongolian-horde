# TODO

## Real manpage

Asciidoc sucks.  I ripped it out, but not I need to format the manpage
the old fashioned way.

## Better resending strategy

We resend only when we run out of buffer space.  Flow might be improved
if we somehow decided to resend older requests once we receive newer
replies.

## Better window sizing

If we have lots of free space, we don't need it.  Therefore, the window
should be smaller.

If we receive duplicates, we were too eager to resend stuff.  Therefore,
the window should be bigger.

These two seem to be in conflict if we don't receive enough stuff from
upstream?


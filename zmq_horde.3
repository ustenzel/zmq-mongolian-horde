.TH ZMQ_HORDE 3 2019-04-03 ZeroMQ
.
.SH NAME
zmq_horde \- start parallel processing 0MQ queue device
.
.SH SYNOPSIS
.
.B #include <zmq_horde.h>

.BI "*int zmq_horde(void *" upstream ", void *" downstream ", void *" horde ", void *" control ");"
.
.SH DESCRIPTION
.
The 
.BR zmq_horde ()
function starts the parallel processing 0MQ queue
device in the current application thread.
.
.PP
The device connects an 
.I upstream
socket to a 
.I downstream
socket through a
birectional 
.I horde 
socket.  Conceptually, requests flow from 
.I upstream 
to
.I horde 
and replies flow back from 
.I horde 
and to 
.IR downstream .  
The device makes sure that exactly one reply is sent for each request,
and in the same order, even if replies from 
.I horde
arrive in arbitrary order, are lost or repeated.
.
.PP
Every request sent to 
.I horde
is prefixed with a message part containing a
sequence number, and an empty message part.  Replies received from 
.I horde 
must begin with the
sequence number of the request they refer to, and and empty message
part, in order to be accepted.
The standard 
.B REQ
and
.B ROUTER
sockets handle this concvention transparently.
.
.PP
Before calling 
.BR zmq_horde (),
you must set any socket options, connect or
bind all the 
.I upstream 
and 
.I downstream 
sockets, and bind the 
.I horde
socket.
In particular, the default values of 
.BR ZMQ_RCVHWM " and " ZMQ_SNDHWM
might be impractically large.  
.BR zmq_horde ()
runs in the current thread and returns only if and when the current
context is closed.
.
.PP
The 
.I upstream
socket shall be a 
.BR ZMQ_PULL ", " ZMQ_DEALER ", " ZMQ_PAIR ", or " ZMQ_SUB
socket.
The 
.I downstream
socket shall be a 
.BR ZMQ_PUSH ", " ZMQ_DEALER ", " ZMQ_PAIR ", or " ZMQ_PUB
socket.
The 
.I horde
socket shall be a 
.B ZMQ_DEALER
socket.
A single socket can be used for both 
.IR upstream " and " downstream
if it is a 
.BR ZMQ_DEALER " or " ZMQ_ROUTER " socket."
Refer to 
.BR zmq_socket (3)
for a description of the available socket types.
.
.PP
If the 
.I control
socket is not NULL,
the horde supports control flow.
If
.B \(dqPAUSE\(dq
is received on this socket, the horde suspends its activities.
If 
.B \(dqRESUME\(dq
is received, it goes on.
If 
.B \(dqTERMINATE\(dq
is received, it acts
as if it received 
.B \(dqSTATISTICS\(dq 
and then terminates smoothly.
If
.B \(dqSTATISTICS\(dq
is received, the horde will reply on the 
.I control
socket with a message containing a
.BR "struct zmq_horde_stats" .
.
.SS The \fIzmq_horde_stats\fP Structure
.
.PP
.in +4n
.EX
struct zmq_horde_stats {
    uint64_t current_requests ; // number of requests currently queued
    uint64_t current_replies ;  // number of replies currenlty queued
    uint64_t current_free ;     // number of free slots in the buffer

    uint64_t num_replies ;      // number of replies sent
    uint64_t num_requests ;     // number of requests received

    uint64_t total_resends ;    // number of repeated requests sent
    uint64_t total_dups ;       // number of duplicate replies received
} ;
.EE
.in 
.PP
.
.SS Example Usage
.
When 
.I horde
is connected to a tcp endpoint, processes on a cluster of
computers can connect to it and perform work one message at a time.
The horde device then looks just like one of these workers, receiving
requests in order and sending replies in order, but the work is really
performed in parallel on many machines.
.
.SH RETURN VALUE
.
The 
.BR zmq_horde ()
function returns 
.B 0 
if 
.B \(dqTERMINATE\(dq
is sent to its control socket.
Otherwise, it returns 
.B \-1
and 
.B errno 
is set to 
.BR ETERM " or " EINTR
(the 0MQ context associated with either of the specified sockets was
terminated).
.
.SH EXAMPLE
.
.SS Create a Parallel Processing Horde
.
.PP
.in +4n
.EX
void *upstream = zmq_socket (context, ZMQ_ROUTER);
assert (upstream);
void *horde = zmq_socket (context, ZMQ_DEALER);
assert (horde);
void *control = zmq_socket (context, ZMQ_SUB);
assert (control);

//  Bind  sockets to TCP ports
assert (zmq_bind (upstream, "tcp://*:5555") == 0);
assert (zmq_bind (horde, "tcp://*:5556") == 0);
assert (zmq_connect (control, "tcp://*:5557") == 0);

// Subscribe to the control socket since we have chosen SUB here
assert (zmq_setsockopt (control, ZMQ_SUBSCRIBE, "", 0));

//  Start the horde, which runs until ETERM
zmq_horde (upstream, upstream, horde, control);

// Clean up
zmq_close (upstream);
zmq_close (horde);
zmq_close (control);
.EE
.in
.PP
.
.SS Set Up a Controller In Another Node, Process or Whatever
.
.PP
.in +4n
.EX
void *control = zmq_socket (context, ZMQ_PUB);
assert (control);
assert (zmq_bind (control, "tcp://*:5557") == 0);

// pause the horde
assert (zmq_send (control, "PAUSE", 5, 0) == 0);

// resume the horde
assert (zmq_send (control, "RESUME", 6, 0) == 0);

// terminate the horde
assert (zmq_send (control, "TERMINATE", 9, 0) == 0);

// check statistics
assert (zmq_send (control, "STATISTICS", 10, 0) == 0);
zmq_msg_t stats_msg;

while (1) {
    assert (zmq_msg_init (&stats_msg) == 0);
    assert (zmq_recvmsg (control, &stats_msg, 0) == sizeof (uint64_t));
    assert (rc == sizeof (uint64_t));
    printf ("Stat: %"PRId64"\n", *(uint64_t*)zmq_msg_data (&stats_msg));
    if (!zmq_msg_get (&stats_msg, ZMQ_MORE))
        break;
    assert (zmq_msg_close (&stats_msg) == 0);
}
assert (zmq_msg_close (&stats_msg) == 0);
zmq_close (control);
.EE
.in
.PP
.
.SH SEE ALSO
.BR zmq_bind (3),
.BR zmq_connect (3),
.BR zmq_socket (3),
.BR zmq (7)
.
.SH AUTHOR
Udo Stenzel, based on the man page for 
.BR zmq_proxy (3)


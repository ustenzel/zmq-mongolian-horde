/* Demo program for Mongolian Horde Device
 *
 * Usage: demo [simple|horde]
 *
 * This is a minimalist program to demostrate the use of zmq_horde().
 * It implements a pipeline pattern which "goes wide" in the middle to
 * distribute work to many processes.  Done naively, results arrive in
 * unpredictable order and may get lost if slave processes crash.  Using
 * zmq_horde() to communicate with the slaves makes sure all the results
 * arrive reliably and in order.
 *
 * We spawn one thread for run_source(), which binds to 'source_port'
 * and keeps sending messages forever.  The main thread binds to
 * 'sink_port' and receives messages.  Once it has received 20000
 * messages, it shuts down 0MQ, which terminates all other threads.
 *
 * Slave processes are also forked from the main thread.  In the simple
 * case, they all connect to 'source_addr' and 'sink_addr' and start
 * processing messages.  The output will be jumbled.  If we kill a slave
 * thread, we will also lose messages.
 *
 * In the horde case, another thread is spawned.  It connects to
 * 'source_addr' and 'sink_addr', and binds 'horde_port', then passes
 * control to zmq_horde().  The slaves now connect to 'horde_addr'.
 * Output arrives in order, and even if a slave is killed, nothing is
 * lost.
 */

#include <inttypes.h>
#include <pthread.h>
#include <signal.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <zmq.h>

#include "zmq_horde.h"

static const char source_addr[] = "tcp://localhost:5555" ; // "ipc://zmq-horde-demo-source" ;
static const char sink_addr[]   = "tcp://localhost:5556" ; // "ipc://zmq-horde-demo-sink" ;
static const char horde_addr[]  = "tcp://localhost:5557" ; // "ipc://zmq-horde-demo-horde" ;
static const char ctrl_addr[]   = "tcp://localhost:5558" ; // "ipc://zmq-horde-demo-control" ;

static const char source_port[] = "tcp://*:5555" ; // "ipc://zmq-horde-demo-source" ;
static const char sink_port[]   = "tcp://*:5556" ; // "ipc://zmq-horde-demo-sink" ;
static const char horde_port[]  = "tcp://*:5557" ; // "ipc://zmq-horde-demo-horde" ;
static const char ctrl_port[]   = "tcp://*:5558" ; // "ipc://zmq-horde-demo-control" ;

int zcheck_at( int rc, int testterm, int status, const char *label, int line, char *format, ... )
{
    va_list ap;
    if (rc != -1) return 0;
    if (testterm && (zmq_errno() == ETERM || zmq_errno() == EINTR)) return 1;
    fprintf( stderr, "[%s:%d] ", label, line );
    va_start(ap,format);
    vfprintf( stderr, format, ap );
    va_end(ap);
    fprintf( stderr, "%s%s\n", zmq_errno()?": ":"", zmq_strerror(zmq_errno()) );
    if (status) exit(status) ;
    return 0;
}

// Checks the return code of a 0MQ function and terminates the program
// with an appropriate error message if the call was not successful.
#define zcheck(rc, status, ...) (void)zcheck_at(rc,0,status,__FUNCTION__,__LINE__,__VA_ARGS__)

// Checks the return code of a 0QM function and runs the following block
// (which will typically contain a 'break' or a 'return') if the call
// was interrupted by a signal or terminated by 0MQ shutting down.
// Terminates the program with an appropriate error message if the call
// was unsuccessful in a different way.
#define zterm(rc, status, ...)     if(zcheck_at(rc,1,status,__FUNCTION__,__LINE__,__VA_ARGS__))

// Terminates the program with an appropriate message if 'cond' is
// false.  If available, the last error from 0MQ is included in the
// message.
#define zassert(cond, ...)      (void)zcheck_at(cond?0:-1,0,EXIT_FAILURE,__FUNCTION__,__LINE__,__VA_ARGS__) 


void *run_source( void *ctx )
{
    void *sk = zmq_socket( ctx, ZMQ_PUSH ) ;
    zassert( sk, "no source socket" ) ;
    int linger = 0 ;
    zmq_setsockopt( sk, ZMQ_LINGER, &linger, sizeof(linger) ) ;

    zcheck( zmq_bind( sk, source_port ), EXIT_FAILURE, "binding source" ) ;
    for( uint32_t i = 0 ;; ++i )
    {
        zmq_msg_t msg ;
        zmq_msg_init_size( &msg, 4 ) ;
        *(uint32_t*)(zmq_msg_data( &msg )) = i ;
        zterm( zmq_msg_send( &msg, sk, 0 ), EXIT_FAILURE, "sending message from source" ) break ;
    }
    zcheck( zmq_close( sk ), EXIT_FAILURE, "closing source socket" ) ;
    return 0 ;
}

int run_worker( void *up, void *down )
{
    int rc = 0 ;
    // We may be receiving multi-part messages, because the horde device
    // will prepend a frame with the sequence number.  We are only
    // interested in the last frame, everything before it should be
    // unchanged.  Therefore, we simply send everything we receive on
    // its way, only when zmq_msg_more() returns 0 (we just got the last
    // frame) do we operate on the data.
    for(;;) {
        zmq_msg_t msg_in ;
        zmq_msg_init( &msg_in ) ;

        zterm( rc=zmq_msg_recv( &msg_in, up, 0 ), EXIT_FAILURE, "receiving request" ) break ;
        zmq_msg_t msg_out ;
        zmq_msg_init_size( &msg_out, 8 ) ;

        // perform expensive computation: takes about 100 microseconds
        usleep( 90 + 20ULL * rand() / RAND_MAX ) ;

        uint32_t i = *(uint32_t*)zmq_msg_data(&msg_in) ;
        ((uint32_t*)zmq_msg_data(&msg_out))[0] = i ;
        ((uint32_t*)zmq_msg_data(&msg_out))[1] = i * 0x9e3779b9 ;

        zterm( rc=zmq_msg_send( &msg_out, down, 0 ), EXIT_FAILURE, "sending result" ) break ;
        zmq_msg_close( &msg_in ) ;
    }
    return rc ;
}

void run_worker_simple() 
{
    void *ctx = zmq_ctx_new() ;
    zassert( ctx, "no context" ) ;

    void *sk1 = zmq_socket( ctx, ZMQ_PULL ) ;
    void *sk2 = zmq_socket( ctx, ZMQ_PUSH ) ;
    zassert( sk1, "no upstream socket" ) ;
    zassert( sk2, "no downstream socket" ) ;

    int hwm = 64 ;
    zmq_setsockopt( sk1, ZMQ_SNDHWM, &hwm, sizeof(hwm) ) ;
    zmq_setsockopt( sk2, ZMQ_SNDHWM, &hwm, sizeof(hwm) ) ;
    zmq_setsockopt( sk1, ZMQ_RCVHWM, &hwm, sizeof(hwm) ) ;
    zmq_setsockopt( sk2, ZMQ_RCVHWM, &hwm, sizeof(hwm) ) ;

    zcheck( zmq_connect( sk1, source_addr ), EXIT_FAILURE, "connecting to source" ) ;
    zcheck( zmq_connect( sk2, sink_addr ), EXIT_FAILURE, "connecting to sink" ) ;
    zterm( run_worker( sk1, sk2 ), EXIT_FAILURE, "running worker" ) ;
    zcheck( zmq_close( sk1 ), EXIT_FAILURE, "closing upstream socket" ) ;
    zcheck( zmq_close( sk2 ), EXIT_FAILURE, "closing downstream socket" ) ;

    zcheck( zmq_ctx_term( ctx ), EXIT_FAILURE, "terminating context" ) ;
}

void run_worker_horde() 
{
    void *ctx = zmq_ctx_new() ;
    zassert( ctx, "no context" ) ;

    void *sk = zmq_socket( ctx, ZMQ_REP ) ;
    zassert( sk, "no horde socket" ) ;

    int hwm = 64 ;
    zmq_setsockopt( sk, ZMQ_SNDHWM, &hwm, sizeof(hwm) ) ;
    zmq_setsockopt( sk, ZMQ_RCVHWM, &hwm, sizeof(hwm) ) ;

    zcheck( zmq_connect( sk, horde_addr ), EXIT_FAILURE, "connecting to horde" ) ;
    zterm( run_worker( sk, sk ), EXIT_FAILURE, "running worker" ) ;
    zcheck( zmq_close( sk ), EXIT_FAILURE, "closing horde socket" ) ;

    zcheck( zmq_ctx_term( ctx ), EXIT_FAILURE, "terminating context" ) ;
}

void *run_horde( void *ctx )
{
    void *upstream = zmq_socket( ctx, ZMQ_PULL ) ;
    void *downstream = zmq_socket( ctx, ZMQ_PUSH ) ;
    void *horde = zmq_socket( ctx, ZMQ_DEALER ) ;
    void *control = zmq_socket( ctx, ZMQ_DEALER ) ;
    zassert( upstream, "no upstream socket" ) ;
    zassert( downstream, "no downstream socket" ) ;
    zassert( horde, "no horde socket" ) ;
    zassert( control, "no horde socket" ) ;

    int hwm = 64 ;
    zmq_setsockopt( horde, ZMQ_SNDHWM, &hwm, sizeof(hwm) ) ;
    zmq_setsockopt( horde, ZMQ_RCVHWM, &hwm, sizeof(hwm) ) ;

    zcheck( zmq_bind( horde, horde_port ), EXIT_FAILURE, "binding horde socket" ) ;
    zcheck( zmq_connect( downstream, sink_addr ), EXIT_FAILURE, "connecting downstream" ) ;
    zcheck( zmq_connect( upstream, source_addr ), EXIT_FAILURE, "connecting upstream" ) ;
    zcheck( zmq_connect( control, ctrl_addr ), EXIT_FAILURE, "connecting control socket" ) ;

    zterm( zmq_horde( upstream, downstream, horde, control ), EXIT_FAILURE, "running horde" ) ;

    zmq_close( control ) ;
    zmq_close( horde ) ;
    zmq_close( downstream ) ;
    zmq_close( upstream ) ;
    return 0 ;
}

enum mode { mode_simple, mode_horde } ;
static const int nslaves = 8 ;

int main( int argc, char **argv )
{
    enum mode mode ;
    if( argc == 2 && !strcmp( argv[1], "simple" ) ) mode = mode_simple ;
    else if( argc == 2 && !strcmp( argv[1], "horde" ) ) mode = mode_horde ;
    else {
        fprintf( stderr, "Usage: %s [simple|horde]\n", argv[0] ) ;
        exit(EXIT_FAILURE) ;
    }

    pid_t slaves[nslaves] ;
    for( int i = 0 ; i != nslaves ; ++i ) {
        slaves[i] = fork() ;
        zassert( slaves[i] != -1, "couldn't create thread" )  ;
        if(!slaves[i]) {
            if( mode == mode_horde ) run_worker_horde() ;
            else run_worker_simple() ;
            exit(EXIT_SUCCESS) ;
        }
    }

    void *ctx = zmq_ctx_new() ;
    zassert( ctx, "no context" ) ;

    pthread_t source, device ;
    zassert( !pthread_create( &source, 0, run_source, ctx ) , "couldn't create thread" ) ;

    if( mode == mode_horde )
        zassert( !pthread_create( &device, 0, run_horde, ctx ), "couldn't create thread" )  ;

    void *sk = zmq_socket( ctx, ZMQ_PULL ) ;
    zcheck( zmq_bind( sk, sink_port ), EXIT_FAILURE, "binding to sink" ) ;
    void *ctrl = zmq_socket( ctx, ZMQ_DEALER ) ;
    zcheck( zmq_bind( ctrl, ctrl_port ), EXIT_FAILURE, "binding to control" ) ;

    int linger = 0 ;
    zmq_setsockopt( sk, ZMQ_LINGER, &linger, sizeof(linger) ) ;
    zmq_setsockopt( ctrl, ZMQ_LINGER, &linger, sizeof(linger) ) ;

    zmq_msg_t msg, m ;
    zmq_msg_init( &msg ) ;
    zmq_msg_init( &m ) ;
    for( uint32_t i = 0 ; i != 20000 ; ++i )
    {
        // Kill one slave to show the effect.  In the simple case, some
        // replies are lost and we instead receive replies with sequence
        // numbers >20000.  In the horde case, everything is fine.
        if( i == 2000 ) { kill( slaves[3], SIGTERM ) ; slaves[3] = 0 ; }

        zterm( zmq_msg_recv( &msg, sk, 0 ), EXIT_FAILURE, "receiving reply" ) break ;
        uint32_t *p = zmq_msg_data(&msg) ;
        printf( "%d: got %d; %08x, expected %08x\n", (int)i, 
                (int)(p[0]), (int)(p[1]),
                (int)(p[0] * 0x9e3779b9) ) ;

        // Get statistics, because we can.  (In the simple case, we obviously can't.)
        if( mode == mode_horde && i % 10 == 0 ) {
            zterm( zmq_send( ctrl, "STATISTICS", 10, 0 ), EXIT_FAILURE, "send" ) ;
            zterm( zmq_msg_recv( &m, ctrl, 0 ), EXIT_FAILURE, "recv" ) ;
            struct zmq_horde_stats *s = zmq_msg_data(&m) ;

            fprintf( stderr, "zmq_horde: %"PRId64" records received, %"PRId64" sent, %"PRId64" resends; "
                             "%"PRId64" replies held, %"PRId64" in flight, %"PRId64" free.\n"
                   , s->num_requests, s->num_replies, s->total_resends
                   , s->current_replies, s->current_requests, s->current_free ) ;
        }
    }
    zmq_msg_close( &m ) ;
    zmq_msg_close( &msg ) ;
    zmq_close( sk ) ;
    zmq_close( ctrl ) ;

    zcheck( zmq_ctx_shutdown( ctx ), EXIT_FAILURE, "shutting down context" ) ;
    for( int i = 0 ; i != nslaves ; ++i )
        if( slaves[i] )
            kill( slaves[i], SIGTERM ) ;
    if( mode == mode_horde ) pthread_join( device, 0 ) ;
    pthread_join( source, 0 ) ;
    zcheck( zmq_ctx_term( ctx ), EXIT_FAILURE, "terminating context" ) ;
    return 0 ;
}

